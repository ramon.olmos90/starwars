$(function(){

    $("#loading").hide();
    consumer_url = 'https://swapi.co/api/people/?format=json';

    $(document).on("click",".link",function(ev){
            d_url = $(this).data("url");
            $.ajax({
                url: d_url,
                beforeSend: function()
                {
                    $('#data-modal').modal('show');
                    $("#data-modal .modal-content  .loading").show();
                    $("#data-modal .modal-content .table").hide();
                },
                success: function(data)
                {
                    $("#data-modal .modal-content  .loading").hide();
                    $("#data-modal .modal-content .table").show();
                    updateNames(data.name,data.height,data.mass,data.skin_color);
                },
                error: function()
                {
                    $("#data-modal .modal-content  .loading").hide();
                    $("#data-modal .modal-content .table").hide();
                }
            });
            ev.preventDefault();
    });

    function updateNames(name,height,mass,skinColor)
    {
        $("#data-modal .name").html(name);
        $("#data-modal .height").html(height);
        $("#data-modal .mass").html(mass);
        $("#data-modal .skinColor").html(skinColor);
    }


    $.ajax({
        url: consumer_url,
        success: function(data){

            $.each(data.results,function(k,value){
                data.results[k]["species"] = data.results[k]["species"][0];
            });

             dif_species = [];
             $('#table').dynatable({
                features: {
                    paginate: false,
                    recordCount: true,
                    sorting: true,
                    search: false
                },
                dataset:
                    {
                        records: data.results
                    },
                bodyRowSelector: 'td',
                rowReader: function(index, td, record) {
                    var td = $(td);
                    if(record.gender == "male")
                    {
                        td.find('.gender').html("MALO");
                    }
                    else if(record.gender == "female")
                    {
                        td.find('.gender').html("BUENO");
                    }
                    else
                    {
                       td.find('.gender').html("NO SE");
                    }
                },
                 writers: {
                     _rowWriter: function(rowIndex, record, columns, cellWriter) {
                         genderIcon = {"male":'<i class="fa fa-male fa-2x" aria-hidden="true"></i>',"female":'<i class="fa fa-female fa-2x" aria-hidden="true"></i>','n/a':'<i class="fa fa-question fa-2x" aria-hidden="true"></i>'}
                         specie = {"https://swapi.co/api/species/1/":'Human',"https://swapi.co/api/species/2/":'Droid'}
                         output = "<tr><td><span class='link' data-url='"+record.url+"'>"+record.name+"</span></td><td>"+specie[record.species]+"</td><td>"+genderIcon[record.gender]+"</td></td></tr>";
                         return output;
                     }
                 },
                 params: {
                     queryRecordCount: 'results.length',
                     totalRecordCount: 'count'
                 }
            });

            var tble = $('#table').data('dynatable');
            tble.paginationPerPage.set(10); // Show 20 records per page
            tble.paginationPage.set(1); // Go to page 5
            tble.process();
        }
    });

});