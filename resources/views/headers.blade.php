@section("header")
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Start Wars - List </title>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset("css/jquery.dynatable.css") }}">
<link rel="stylesheet" href="{{ asset("css/font-awesome.min.css") }}">

<script
        src="https://code.jquery.com/jquery-1.9.1.min.js"
        integrity="sha256-wS9gmOZBqsqWxgIVgA8Y9WcQOa7PgSIX+rPA0VL2rbQ="
        crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.js"></script>
<script src="{{ asset("js/jquery.dynatable.js") }}"></script>
<script src="{{ asset("js/app.js") }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset("css/util.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("css/main.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("css/app.css") }}">
@endsection