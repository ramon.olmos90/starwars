@extends("layout.main")
@section("header")
    @include("headers")
@endsection
@section("content")
<body>

<div id="content" class="container">

    <h2 class="title">Lista de Personajes</h2>

    <table id="table" class="table">
        <thead>
        <tr class="table100-head">
        <th data-dynatable-column="name" class="column1">Nombre</th>
        <th data-dynatable-column="species" class="column2">Especie</th>
        <th data-dynatable-column="gender" class="gender column3">Genero</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <div id="data-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Detalle del Personaje</h4>
                </div>
                <div class="modal-body">
                    <div class="container-fluid loading text-center">
                    <i class="fa fa-spinner fa-spin fa-5x "></i>
                    </div>
                    <table class="table">
                        <tr><th>Nombre</th><td class="name"></td></tr>
                        <tr><th>Altura</th><td class="height"></td></tr>
                        <tr><th>Masa</th><td class="mass"></td></tr>
                        <tr><th>Color de Piel</th><td class="skinColor"></td></tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>

        </div>
    </div>

</div>

</body>
@endsection
